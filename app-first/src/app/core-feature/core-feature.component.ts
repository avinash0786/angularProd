import { Component } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-coreFeature',
  templateUrl: './core-feature.component.html',
})
export class CoreFeatureComponent {
  constructor(private dataServ: DataService) {}

  fileToUpload: File | null = null;

  handleFileInput(event: any) {
    console.log(event);
    this.fileToUpload = event.target.files.item(0);
  }

  uploadFileToActivity() {
    this.dataServ.postFile(this.fileToUpload).subscribe(
      (data) => {
        console.log('File uploaded :SUCCESS');
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
