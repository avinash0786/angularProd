import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { from, of,timer,interval } from 'rxjs';
import {
  concatMap,
  debounceTime,
  delay,
  distinctUntilChanged,
  filter,
  map,
  mergeAll,
  mergeMap,
  pluck,
  switchAll,
  switchMap, take,
} from 'rxjs/operators';
import { DataService } from '../../services/data.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css'],
})
export class MapsComponent implements OnInit, AfterViewInit {
  constructor(private dataServ: DataService) {}

  ngOnInit(): void {
    const myTimer=interval(1000).pipe(delay(200),take(10));
    myTimer.subscribe(res=>console.log(res))
    const source = from(['apple', 'mango', 'papaya']);
    //2 subscribe implementation
    source.subscribe((data) =>
      this.getData(data).subscribe((final) => {
        console.log(final);
        this.dataServ.printEle(final, 'first');
      })
    );

    //map operator use
    source
      .pipe(map((res) => this.getData(res)))
      .subscribe((final) =>
        final.subscribe((out) => this.dataServ.printEle(out, 'second'))
      );

    //map + mergeAll
    source
      .pipe(
        map((data) => this.getData(data)),
        mergeAll()
      )
      .subscribe((final) => this.dataServ.printEle(final, 'third'));

    //mergeMap implementation
    source
      .pipe(mergeMap((data) => this.getData(data)))
      .subscribe((final) => this.dataServ.printEle(final, 'forth'));

    //switch Map: flattening operator,
    //map op
    source
      .pipe(map((data) => this.getData(data)))
      .subscribe((sec) =>
        sec.subscribe((res) => this.dataServ.printEle(res, 'm1'))
      );
    //map+switchAll
    source
      .pipe(
        map((data) => this.getData(data)),
        switchAll()
      )
      .subscribe((res) => this.dataServ.printEle(res, 'm2'));

    //switchMap
    source
      .pipe(switchMap((data) => this.getData(data)))
      .subscribe((res) => this.dataServ.printEle(res, 'm3'));

    //concatMap
    source
      .pipe(concatMap((data) => this.getData(data)))
      .subscribe((res) => this.dataServ.printEle(res, 'm4'));

    //mergeMap
    source
      .pipe(mergeMap((data) => this.getData(data)))
      .subscribe((res) => this.dataServ.printEle(res, 'm5'));

    //implementing search functionality using the switchMap, cancelling operator
  }

  @ViewChild('myForm') myForm!: NgForm;
  //returns the observable
  getData(fruit: string) {
    return of(fruit + ' Delivered').pipe(delay(1000));
  }
  searchResults: string[] = [''];
  searchCount!: number;
  ngAfterViewInit(): void {
    const formValue = this.myForm.valueChanges;
    formValue
      ?.pipe(
        filter(() => this.myForm.valid || false),
        pluck('searchTerm'),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap((dat) => this.dataServ.userNames(dat))
      )
      ?.subscribe((res) => {
        console.log(res);
        this.searchCount = res.length;
        this.searchResults = res;
      });
  }
}
