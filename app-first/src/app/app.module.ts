import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ServerElemenComponent } from './core-feature/server-elemen/server-elemen.component';
import { HighlightDirective } from './core-feature/directives/highlight.directive';
import { AuthGuard } from './services/auth-guard.service';
import { CanDeactivateGuard } from './profile-module/profile/can-leave.service';
import { AuthInceptorService } from './services/auth-inceptor.service';
import { SharedModule } from './shared/shared.module';
import { HomePageComponent } from './shared/home-page/home-page.component';
import { AccessGuardService } from './services/access-guard.service';
import {NavbarComponent} from "./shared/navbar/navbar.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { MatSnackBarModule} from "@angular/material/snack-bar";
import {GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule} from "angularx-social-login";
import {environment} from "../environments/environment";

@NgModule({
  declarations: [
    AppComponent,
    ServerElemenComponent,
    HighlightDirective,
    HomePageComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    MatSnackBarModule,
    SocialLoginModule,
    NgxsModule.forRoot([]),
    NgxsLoggerPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot()
  ],
  providers: [
    AuthGuard,
    CanDeactivateGuard,
    AccessGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInceptorService,
      multi: true,
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.GOOGLE_CONSUMER_KEY),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
