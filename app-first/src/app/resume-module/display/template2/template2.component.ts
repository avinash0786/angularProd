import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-template2',
  templateUrl: './template2.component.html',
  styleUrls: ['./template2.component.css'],
})
export class Template2Component implements OnInit {
  constructor() {}

  ngOnInit(): void {
    console.log('Template one init');
  }
  showResume: boolean = true;
  @Input() inpResumeData!: any;
}
