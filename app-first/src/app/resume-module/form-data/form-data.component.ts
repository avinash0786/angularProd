import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { DataService } from '../../services/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form-data',
  templateUrl: './form-data.component.html',
  styleUrls: ['./form-data.component.css'],
})
export class FormDataComponent implements OnInit {
  private editSubs!: Subscription;
  firstFormGroup!: FormGroup;
  secondFormGroup!: FormGroup;
  isLinear = false;

  constructor(
    private dataServ: DataService,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    console.log('Form data comp loaded');
    this.createNewForm();
    this.editSubs = this.dataServ.editResumeSelect.subscribe((data: any) => {
      console.log('Load edit from req rec for: ' + data);
      this.loadResumeData(data);
    });
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required],
    });
    this.dataServ.previewResumeSelect.subscribe((data) => {
      this.showForm = false;
    });
    this.dataServ.templateSelect.subscribe((data) => {
      this.showForm = false;
    });
    this.dataServ.closeForm.subscribe(() => {
      this.showForm = false;
    });
  }

  @Output() sendFormResume = new EventEmitter<any>();
  ////////////    FormGroups for dynamic froms    ///////////////////

  //listening to edit request emit

  loadResumeData(id: any) {
    this.currentResumeID = id;

    this.showForm = false;
    this.showLoadMessage = true;
    this.dataServ.getOneCV(id).subscribe(
      (data: any) => {
        console.log('Single CV data recieved');
        console.log(data);
        //here we will open the resume
        this.showForm = true;
        console.log('Opening from for edit');
        this.showLoadMessage = false;
        this.editResume(data.body);
      },
      (err) => {
        console.log(err);
      }
    );
  }
  resumeFormData!: FormGroup;
  showForm: boolean = false;
  showLoadMessage: boolean = false;
  currentResumeID!: string;
  loadMessage: String = 'Fetching Loaded data...';

  disableSubmit: boolean = false;

  //creating a new form
  clickShowForm() {
    this.showForm = true;
    this.dataServ.templatesShow.next(false);
    this.resumeFormData.reset();
  }
  createNewForm() {
    this.currentResumeID = '';
    console.log('New form creating'); //hobbies is added through the chips;
    this.resumeFormData = new FormGroup({
      resumeName: new FormControl(null),
      basicDetails: new FormGroup({
        firstName: new FormControl(null),
        lastName: new FormControl(null),
        profession: new FormControl(null),
        email: new FormControl(null),
        phone: new FormControl(null),
        city: new FormControl(null),
        state: new FormControl(null),
        pinCode: new FormControl(null),
      }),
      skills: new FormArray([]),
      projects: new FormArray([]),
      experiences: new FormArray([]),
      accomplishments: new FormArray([]),
      education: new FormArray([]),
      certification: new FormArray([]),
      socialLink: new FormGroup({
        linkedInLink: new FormControl(null),
        gitHubLink: new FormControl(null),
      }),
    });
  }

  editResume(formData: any) {
    //changing basic details
    console.log('Edit form OPENED SUCCESSFULLY');
    let currentForm = this.resumeFormData;
    let tempBasicD = formData.basicDetails;

    currentForm.get('resumeName')?.setValue(formData.resumeName);
    //setting the basic details
    currentForm
      .get('basicDetails')
      ?.get('firstName')
      ?.setValue(tempBasicD.firstName);
    currentForm
      .get('basicDetails')
      ?.get('lastName')
      ?.setValue(tempBasicD.lastName);
    currentForm
      .get('basicDetails')
      ?.get('profession')
      ?.setValue(tempBasicD.profession);
    currentForm.get('basicDetails')?.get('email')?.setValue(tempBasicD.email);
    currentForm.get('basicDetails')?.get('phone')?.setValue(tempBasicD.phone);
    currentForm.get('basicDetails')?.get('city')?.setValue(tempBasicD.city);
    currentForm.get('basicDetails')?.get('state')?.setValue(tempBasicD.state);
    currentForm
      .get('basicDetails')
      ?.get('pinCode')
      ?.setValue(tempBasicD.pinCode);

    //adding the skills
    (<FormArray>this.resumeFormData.get('skills')).clear(); //removing all controls
    for (let skl of formData.skills) {
      (<FormArray>this.resumeFormData.get('skills')).push(
        new FormGroup({
          name: new FormControl(skl.name),
          level: new FormControl(skl.level),
        })
      );
    }
    //adding the projects
    (<FormArray>this.resumeFormData.get('projects')).clear(); //removing all controls
    for (let pro of formData.projects) {
      (<FormArray>this.resumeFormData.get('projects')).push(
        new FormGroup({
          projectName: new FormControl(pro.projectName),
          link: new FormControl(pro.link),
          startDate: new FormControl(new Date(pro.startDate)),
          endDate: new FormControl(new Date(pro.endDate)),
          desc: new FormControl(pro.desc.join()),
        })
      );
    }

    //adding the experiences/jobs

    (<FormArray>this.resumeFormData.get('experiences')).clear(); //removing all controls
    for (let job of formData.experiences) {
      (<FormArray>this.resumeFormData.get('experiences')).push(
        new FormGroup({
          jobTitle: new FormControl(job.jobTitle),
          employer: new FormControl(job.employer),
          city: new FormControl(job.city),
          startDate: new FormControl(new Date(job.startDate)),
          endDate: new FormControl(new Date(job.endDate)),
          desc: new FormControl(job.desc.join()),
        })
      );
    }

    //adding the education

    (<FormArray>this.resumeFormData.get('education')).clear(); //removing all controls
    for (let edu of formData.education) {
      (<FormArray>this.resumeFormData.get('education')).push(
        new FormGroup({
          schoolName: new FormControl(edu.schoolName),
          location: new FormControl(edu.location),
          degree: new FormControl(edu.degree),
          fieldOfStudy: new FormControl(edu.fieldOfStudy),
          startDate: new FormControl(new Date(edu.startDate)),
          endDate: new FormControl(new Date(edu.endDate)),
          marks: new FormControl(edu.marks),
        })
      );
    }

    //adding the certification
    (<FormArray>this.resumeFormData.get('certification')).clear(); //removing all controls
    for (let cert of formData.certification) {
      (<FormArray>this.resumeFormData.get('certification')).push(
        new FormGroup({
          name: new FormControl(cert.name),
          description: new FormControl(cert.description),
          link: new FormControl(cert.link),
          date: new FormControl(new Date(cert.date)),
        })
      );
    }

    //adding the social links
    (<FormArray>this.resumeFormData.get('socialLink'))
      .get('linkedInLink')
      ?.setValue(formData.socialLink.linkedInLink); //removing all controls
    (<FormArray>this.resumeFormData.get('socialLink'))
      .get('gitHubLink')
      ?.setValue(formData.socialLink.gitHubLink); //removing all controls

    //adding the hobbies
    this.hobbies = [];
    formData.hobbies.forEach((hob: any) => this.hobbies.push({ name: hob }));
    //finally the from data is changed

    this.showLoadMessage = false;

    //end of the fn block
  }

  //updating current resume state
  updateResume() {
    console.log('Update request for: ' + this.currentResumeID);
    let finalData = this.resumeFormData.value;
    finalData['hobbies'] = this.getHobbiesArray();
    console.log(finalData);
    this.dataServ
      .updateResume(finalData, this.currentResumeID)
      .subscribe((respo: any) => {
        console.log('Update resume response');
        this.dataServ.openSnackBar('Resume Updated Successfully', 'Ok');
        this.dataServ.refreshResume.next('refresh Resume');
        console.log(respo);
      });
  }

  //saving form data
  saveResumeDate() {
    this.disableSubmit = true;
    console.log('Saving form DATA');
    let finalData = this.resumeFormData.value;
    finalData['hobbies'] = this.getHobbiesArray();
    console.log(finalData);
    this.dataServ.addResume(finalData).subscribe((respo: any) => {
      this.disableSubmit = false;
      console.log('New resume aded response');
      console.log(respo);
      this.dataServ.openSnackBar('Resume Saved Successfully', 'Ok');
      console.log('emitting to refrest available resume');
      this.dataServ.refreshResume.next('refresh Resume');
    });
    console.log(finalData);
  }

  deleteResume() {
    console.log('Current resume ID [delete]: ' + this.currentResumeID);
    this.dataServ.deleteCV(this.currentResumeID).subscribe((respo: any) => {
      console.log('Delete resume request send');
      console.log(respo);
      this.dataServ.refreshResume.next('refresh Resume');
      this.dataServ.openSnackBar('Resume Deleted Successfully', 'Ok');
      this.showForm = false;
    });
  }
  downloadResume() {}
  //adding sub components of a block
  onAddSkill() {
    const nayaSkill = new FormGroup({
      name: new FormControl(null),
      level: new FormControl(null),
    });
    (<FormArray>this.resumeFormData.get('skills')).push(nayaSkill);
  }

  onAddProject() {
    const nayaProject = new FormGroup({
      projectName: new FormControl(null),
      link: new FormControl(null),
      startDate: new FormControl(),
      endDate: new FormControl(),
      desc: new FormControl(null),
    });
    (<FormArray>this.resumeFormData.get('projects')).push(nayaProject);
  }

  onAddExperience() {
    const nayaJob = new FormGroup({
      jobTitle: new FormControl(null),
      employer: new FormControl(null),
      city: new FormControl(null),
      startDate: new FormControl(),
      endDate: new FormControl(),
      desc: new FormControl(null),
    });
    (<FormArray>this.resumeFormData.get('experiences')).push(nayaJob);
  }

  onAddEducation() {
    const nayaEdu = new FormGroup({
      schoolName: new FormControl(null),
      location: new FormControl(null),
      degree: new FormControl(null),
      fieldOfStudy: new FormControl(),
      startDate: new FormControl(),
      endDate: new FormControl(),
      marks: new FormControl(null),
    });
    (<FormArray>this.resumeFormData.get('education')).push(nayaEdu);
  }

  onAddCertificate() {
    const nayaCert = new FormGroup({
      name: new FormControl(null),
      description: new FormControl(null),
      link: new FormControl(null),
      date: new FormControl(),
    });
    (<FormArray>this.resumeFormData.get('certification')).push(nayaCert);
  }
  //removing individual control by index
  removeIndivSkill(index: number) {
    console.log('Removing skill at: ' + index);
    (<FormArray>this.resumeFormData.get('skills')).removeAt(index);
  }
  //removing the control
  onRemoveProject(index: number) {
    (<FormArray>this.resumeFormData.get('projects')).removeAt(index);
  }
  onRemoveExperience(index: number) {
    (<FormArray>this.resumeFormData.get('experiences')).removeAt(index);
  }
  onRemoveEducation(index: number) {
    (<FormArray>this.resumeFormData.get('education')).removeAt(index);
  }
  onRemoveCertificate(index: number) {
    (<FormArray>this.resumeFormData.get('certification')).removeAt(index);
  }
  //returning form controls for iteration
  getSkillsControl() {
    return (this.resumeFormData.get('skills') as FormArray).controls;
  }
  getProjectsControl() {
    return (this.resumeFormData.get('projects') as FormArray).controls;
  }
  getExperienceControl() {
    return (this.resumeFormData.get('experiences') as FormArray).controls;
  }
  getEducationControl() {
    return (this.resumeFormData.get('education') as FormArray).controls;
  }
  getCertificationControl() {
    return (this.resumeFormData.get('certification') as FormArray).controls;
  }

  resetFormResume() {
    console.log('resetting form data');
    this.dataServ.openSnackBar('Resume Cleared', 'Ok');
    this.resumeFormData.reset();
  }

  ///////////////////////////////////////////////////////////////////
  signupForm: FormGroup = new FormGroup({});
  formDataRedundent: any = {
    userName: 'apple is bllue',
    resumeName: 'my first resume',
    socialWebsite: {
      linkedin: {
        link: 'link of linkedin',
      },
      github: {
        link: 'link of github',
      },
    },
    skill: [
      { name: 'first skill', level: 3 },
      { name: 'second skill', level: 5 },
    ],
    accomplishments: [
      {
        name: 'first acc',
        description: 'first desc',
        link: 'link of acc',
        date: '21 jan 2021',
      },
      {
        name: 'second acc',
        description: 'second desc',
        link: 'link of acc',
        date: '21 jan 2021',
      },
    ],
    projects: [
      {
        projectName: 'live poll',
        link: 'link of project',
        startDate: '14 June 2021',
        endDate: '23 may 2022',
        desc: ['project desc one', 'project desc 2'],
      },
    ],
    basicInfo: {
      firstName: 'avi',
      lastName: 'k',
      profession: 'assoc dev',
      city: 'kalimpong',
      state: 'WB',
      zipCode: '458795',
      phone: '+19sas',
      email: 'email my',
    },
    jobs: [
      {
        jobTitle: 'ASD',
        employer: 'Venturepact',
        city: 'Jalandhar',
        startDate: '14 June 2021',
        endDate: '23 may 2022',
        desc: ['tihis is the description of the job ', 'secodn dec'],
      },
      {
        jobTitle: 'SD',
        employer: 'Postman',
        city: 'Jalandhar',
        startDate: '14 June 2022',
        endDate: '23 may 2022',
        desc: ['tihis is the description of the job ', 'secodn dec'],
      },
    ],
    education: [
      {
        schoolName: 'APS',
        location: 'chandimandir',
        degree: 'B.tech',
        fieldOfStudy: 'CSE',
        startDate: '14 June 2022',
        endDate: '23 may 2022',
        marks: 89.5,
      },
    ],
    hobbies: ['football', 'basketball'],
    certifications: [
      {
        name: 'mastering Angular 2021',
        description: 'this is the desc',
        link: 'http://link',
        date: '12 dec 2021',
      },
    ],
  };

  //sample code for date range picke
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });

  range2 = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });

  range3 = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });

  ///code for chip hobbie selection
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  hobbies: hobbies[] = [];

  getHobbiesArray() {
    let hobs = this.hobbies.map((data) => data.name);
    console.log(hobs);
    return hobs;
  }
  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.hobbies.push({ name: value });
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(fruit: hobbies): void {
    const index = this.hobbies.indexOf(fruit);
    if (index >= 0) {
      this.hobbies.splice(index, 1);
    }
  }

  sendData() {
    this.sendFormResume.emit('sample data');
  }
  public ngOnDestroy(): void {
    this.editSubs?.unsubscribe();
  }

  //  HERE WILL BE THE STEPPER CODE
}
export interface hobbies {
  name: string;
}
