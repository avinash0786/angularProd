import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  NgForm,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-learnforms',
  templateUrl: './learnforms.component.html',
  styleUrls: ['./learnforms.component.css'],
})
export class LearnformsComponent implements OnInit {
  constructor() {}

  signupForm!: FormGroup;

  ngOnInit(): void {
    console.log('Form template driven and reactive implementation');
    //in a form group we need to add different controls
    this.signupForm = new FormGroup({
      userName: new FormControl('testUser', Validators.required),
      //we can form nested by grouping the inputs
      nameData: new FormGroup({
        firstName: new FormControl('Avinash', Validators.required),
        lastName: new FormControl('Kumar', Validators.required),
      }),
      email: new FormControl('avi@gmil.com', [
        Validators.required,
        Validators.email,
      ]),
      hobbies: new FormArray([]),
    });
  }

  @ViewChild('myForm') samForm!: NgForm;
  showData(form: NgForm) {
    console.log('Data submit');
    console.log(this.samForm);
  }

  //REACTIVE FORMS MODULE
  formSubmit() {
    console.log(this.signupForm);
    console.log(this.signupForm.value);
  }
  onAddhobbie() {
    const hob = new FormControl(null);
    (<FormArray>this.signupForm.get('hobbies')).push(hob);
  }

  getControls() {
    return (this.signupForm.get('hobbies') as FormArray).controls;
  }
}
