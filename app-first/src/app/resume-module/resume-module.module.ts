import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSliderModule} from "@angular/material/slider";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatCardModule} from "@angular/material/card";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatIconModule} from "@angular/material/icon";
import {MatChipsModule} from "@angular/material/chips";
import {MatStepperModule} from "@angular/material/stepper";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {PDFExportModule} from "@progress/kendo-angular-pdf-export";


import {ResumeRoutingModule} from "./resume.routing.module";
import {ResumeComponent} from "./resume.component";
import {DisplayComponent} from "./display/display.component";
import {FormDataComponent} from "./form-data/form-data.component";
import {DefaultComponent} from "./display/default/default.component";
import {Template1Component} from "./display/template1/template1.component";
import {Template2Component} from "./display/template2/template2.component";
import {LearnformsComponent} from "./learnforms/learnforms.component";
import {PlaceholderDirective} from "../shared/placeholder.directive";


@NgModule({
  declarations: [
    ResumeComponent,
    DisplayComponent,
    FormDataComponent,
    DefaultComponent,
    Template1Component,
    Template2Component,
    LearnformsComponent,
    PlaceholderDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ResumeRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSliderModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatSnackBarModule,
    MatIconModule,
    MatChipsModule,
    MatStepperModule,
    PDFExportModule,
  ],
  exports:[CommonModule],
  entryComponents:[
    Template1Component,
    Template2Component,
  ]
})
export class ResumeModuleModule { }
