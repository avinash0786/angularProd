import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "../services/auth-guard.service";
import {ResumeComponent} from "./resume.component";

const routes:Routes=[
  {
    path:'',
    component:ResumeComponent,
    canActivate: [AuthGuard],
  }
]
@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})
export class ResumeRoutingModule {}
