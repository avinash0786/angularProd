import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "../services/auth-guard.service";
import {ProfileComponent} from "./profile/profile.component";
import {CanDeactivateGuard} from "./profile/can-leave.service";

const routes:Routes=[
  {
    path:'',
    component:ProfileComponent,
    canActivate: [AuthGuard],
    canDeactivate: [CanDeactivateGuard],
  }
]
@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})
export class ProfileRoutingModule {}
