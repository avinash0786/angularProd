import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {DataService} from "./data.service";
import {take, map} from "rxjs/operators";
@Injectable()
export class AuthGuard implements CanActivate{
  constructor(protected router:Router,private dataServ:DataService) {
  }
  //if there is no auth key we need to login
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
    console.log("User auth key: "+localStorage.getItem('auth'))
    console.log("User Mobile verification: "+    this.dataServ.isMobileVerified)

    if (localStorage.getItem('auth')) {
      console.log('checking user mobile verification in auth guard')
      //we are checking if mobile is verified or not
      if (!this.dataServ.isMobileVerified){
        console.log('NOT VERIFIED:Fetching data from BE,  redirecting to mobile verification');
        //we are returning the entire observable with the logic, it will be evaluated accordingly
        return this.dataServ.loadProfile()
          .pipe(
            take(1),
            map((data:any)=>{
              console.log('resolving mobile verification status');
              console.log(data)
              if (data.body.isMobileVerified) {
                console.log('Mobile verified, allowing access')
                return true;
              }
              else {
                console.log('Mobile NOT verified, redirecting to Verify Mobile')
                // this.router.navigate(['/auth', 'verifyOtp']);
                return this.router.createUrlTree(['/auth', 'verifyOtp']);
              }
            })
          )
          // .subscribe({
          //   next:(data:any)=>{
          //     console.log('AuthGuard: profile data received');
          //     console.log(data);
          //     this.dataServ.isMobileVerified=data.body.isMobileVerified;
          //     //redirecting according to mobile verification state
          //     if (this.dataServ.isMobileVerified) {
          //       console.log('Mobile verified, redirecting to Dashboard')
          //       return this.router.navigate(['/dashboard']);
          //     }
          //     else {
          //       console.log('Mobile NOT verified, redirecting to Verify Mobile')
          //       return this.router.navigate(['/auth', 'verifyOtp']);
          //     }
          //   },
          //   error:err => {
          //     console.log('AuthGuard: Error retrieving profile data');
          //     console.log(err);
          //     return this.router.navigate(['/auth/login']);
          //   }
          // })
      }else
        return true;
    }
    else {
      console.log("Auth guard: redirect to LOGIN")
      return this.router.navigate(['/auth/login']);
    }
  }

}
