import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AccessGuardService implements CanActivate {
  constructor(protected router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    console.log('router data');
    console.log(
      'Allow not-logged access, User auth key: ' + localStorage.getItem('auth')
    );
    if (!localStorage.getItem('auth')) return true;
    else {
      console.log('Auth guard: redirect to Profile');
      return this.router.navigate(['/dashboard']);
    }
  }
}
