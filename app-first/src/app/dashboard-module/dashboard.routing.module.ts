import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "../services/auth-guard.service";
import {DashboardComponent} from "./dashboard.component";
import {Feature01Component} from "./feature01/feature01.component";
import {LandingPageComponent} from "./landing-page/landing-page.component";

const routes:Routes=[
  {
    path:'',
    component:DashboardComponent,
    canActivate: [AuthGuard],
    children:[
      {path:'',component:LandingPageComponent},
      {path:'feature01',component:Feature01Component}
    ]
  }
]
@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})
export class DashboardRoutingModule {}
