import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  constructor(
    private dataService: DataService,
    private router: Router,
    private socialAuthService: SocialAuthService
  ) {}

  ngOnInit(): void {}
  disableRegister: boolean = false;
  showMessage: boolean = false;
  regProcessMessage: string = 'Checking Username';
  out: any = 'Default';

  check() {
    this.dataService.checkCon().subscribe((data) => {
      console.log(data);
      this.out = data;
      console.log(this.out.success);
      this.out = this.out.response;
    });
  }
  fname: string = '';
  lname: string = '';
  userName: string = '';
  email: string = '';
  password: string = '';


  registerUser() {
    if (this.fname=="" || this.lname=="" || this.userName=="" || this.email=="" || this.password==""){
      this.showMessage = true;
      this.regProcessMessage = 'Please enter required details !';
      setTimeout(() => {
        this.showMessage = false;
      }, 2000);
      return;
    }
    this.showMessage = true;
    this.disableRegister = true;
    this.regProcessMessage = 'Registering User';
    console.log('Register user ng called');
    this.dataService
      .registerUser({
        fname: this.fname,
        lname: this.lname,
        userName: this.userName,
        email: this.email,
        pswd: this.password,
      })
      .subscribe({
        next: (data: any) => {
          console.log('data recieved REGISTER');
          console.log(data);
          this.regProcessMessage =
            'Registered Successfully, Redirect login :-)';
          localStorage.setItem('auth', JSON.stringify(data.body.jwt));
          console.log('Setting auth key in Local storage');
          console.log(localStorage.getItem('auth'));
          this.dataService.userStatus.emit(true);
          this.router.navigate(['/profile']);

          setTimeout(() => {
            this.showMessage = false;
          }, 3000);
        },
        error: (err) => {
          console.log(err);
          this.disableRegister = false;
          this.regProcessMessage = 'Error Registering user !';
          console.log('error occured in Regsitering User');
          setTimeout(() => {
            this.showMessage = false;
          }, 3000);
        },
      });
  }

  checkUserAvl(event: any) {
    this.showMessage = true;
    this.regProcessMessage = 'Checking UserName';
    console.log(event.target.value);
    if (event.target.value == '') {
      this.showMessage = false;
      return;
    }
    this.dataService.checkUserName(event.target.value).subscribe({
      next: (data) => {
        console.log('data recieved CHECK user');
        console.log(data);
        // @ts-ignore
        if (data.body.availableUser) {
          this.disableRegister = false;
          console.log('User available for use');
          this.regProcessMessage = 'UserName Available 😄';
        } else {
          this.disableRegister = true;
          console.log('User NOT available for use');
          this.regProcessMessage = 'UserName NOT Available 😔';
        }
        setTimeout(() => {
          this.showMessage = false;
        }, 5000);
      },
      error: (err) => {
        console.log(err);
        this.regProcessMessage = 'Error Checking user !';
        console.log('error occured in checking User');
      },
    });
  }

  loginWithGoogle(): void {
    console.log('Signup with google start');
    this.socialAuthService
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((res) => {
        this.showMessage = true;
        this.regProcessMessage = 'oAuth Signning Up User';

        console.log(res);
        this.dataService
          .verifyAccessToken({ accessToken: res.authToken, email: res.email })
          .subscribe({
            next: (data: any) => {
              console.log('Data rec: from oAuth route');
              console.log(data);
              console.log(data.body.loginRedirect);
              if (data.body.loginRedirect) {
                this.regProcessMessage =
                  'oAuth Signup success, redirect Profile';
                this.dataService.JWT_TOKEN = data.body.jwt;
                this.regProcessMessage = 'User Logged in, Redirect profile :-)';
                localStorage.setItem('auth', JSON.stringify(data.body.jwt));
                this.dataService.userStatus.emit(true);
                this.router.navigate(['/profile']);
                this.regProcessMessage =
                  'oAuth Login success, redirect Profile';
              } else {
                this.regProcessMessage = 'Error Signing up user !';
              }
            },
            error: (err) => {
              console.log('error in oauth');
            },
          });
      });
  }
}
