import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-forgot-pswd',
  templateUrl: './forgot-pswd.component.html',
  styleUrls: ['./forgot-pswd.component.css'],
})
export class ForgotPswdComponent implements OnInit {
  constructor(private dataServ: DataService) {}
  email: string = '';
  showMessage: boolean = false;
  logProcessMessage: string = 'Sending Mail wait  ';
  ngOnInit(): void {
    console.log('Forgot Password module start');
  }
  sendToken() {
    console.log('Sending the token');
    this.showMessage = true;
    this.logProcessMessage = 'Sending Mail wait';
    this.dataServ.sendGenToken({ email: this.email }).subscribe({
      next: (data: any) => {
        this.logProcessMessage = data.body.info;
        console.log('Generate token request recieved');
        console.log(data);
      },
      error: (err) => {
        console.log('Generate token Error');
        this.logProcessMessage = 'Retry: Error sending mail';
      },
    });
  }
}
