require("dotenv").config();

let ngUser = require("../models/ngData");
let ngCV = require("../models/resume");
let tokens = require("../models/pswdToken");
let otp = require("../models/otp");

let bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
const axios = require("axios");
const sgMail = require("@sendgrid/mail");

const smsAccountSid = process.env.TWILIO_ACCOUNT_SID;
const smsAuthToken = process.env.TWILIO_AUTH_TOKEN;
const smsClient = require('twilio')(smsAccountSid, smsAuthToken);

sgMail.setApiKey(process.env.SG_API_KEY);

//sending sms otp.js and verify
exports.sendSMS=async function(req,res){
  console.log('send otp req received')
  console.log(req.body)
  const receiver=req.body.receiver;
  try{
    jwt.verify(
        req.headers["authorization"].slice(1, -1),
        "mysecretKey",
        async (err,authData)=>{
          if (err) {
            console.log(err);
            return res.sendStatus(404);
          }
          let ans = await ngUser.findOne({ userName: authData.userId }).lean();
          console.log(ans);
          if (ans == null) {
            console.log("User not found");
            return res.status(401).json({
              status: false,
              info: "No account is associated with this user !",
            });
          }else {
            //saving the otp in db, and sending user the otp
            const generatedOtp=Math.random().toString().slice(-5);
            let nayaOtp=new otp({
              userName: authData.userId,
              expiry:new Date().setMinutes(new Date().getMinutes() + 10),
              otp:generatedOtp
            })
            nayaOtp.save()
                .then(result=>{
                  console.log('Data saved in DB');
                  console.log(result);
                  //sending the otp to user
                  smsClient.messages
                      .create({body: `OTP to verify Mobile: Enter this OTP within then minutes ${generatedOtp} | ये पिन 10 मिनट्स के अंदर लिखे ${generatedOtp}`, from: '+15818900669', to: '+917889152578'})
                      .then(message => {
                        console.log('SMS sent');
                        console.log(message.sid);
                        res.status(200).json({
                          response:message,
                          otpID:result._id
                        })
                      })
                })
          }
        }
    )
  }
  catch (e) {
    console.log(e)
    console.log("error sending the SMS");
    return res.status(401).json({
      status: false,
      info: "Exception: Failed to send SMS",
    });
  }
}

//verifying the otp.js
exports.verifyOtp=async function(req,res){
  console.log(req.body);
  if (!req.body.id || !req.body.otp) {
    return res.status(401).json({
      status: false,
      info: "Incorrect Credentials provided",
    });
  }
  try {

    const curOtpId=req.body.id;
    const otpDoc=await otp.findById(curOtpId).lean();
    console.log(otpDoc)

    if (otpDoc == null) {
      console.log("Incorrect ID token");
      return res.status(401).json({
        status: false,
        info: "Incorrect id token",
      });
    }else {
      //now verify the id token
      if (new Date(otpDoc.expiry) < Date.now()) {
        console.log("OTP hvas been expired");
        return res.status(404).json({
          status: false,
          info: "OTP token has been EXPIRED",
        });
      }

      //matching the cur otp and otp saved in DB
      if (otpDoc.otp===req.body.otp){
        // otp.deleteOne({_id:curOtpId}) //deleting cur otp document
        //updating user mobileVerification status
        console.log('User to update: '+otpDoc.userName)
        ngUser.updateOne({userName:otpDoc.userName}, {$set: {mobileVerified: true}})
            .then(answer=>{
              console.log(answer)
              console.log('User mobile verification status updated')
              smsClient.messages
                  .create({body: `OTP verification successfull | ओटीपी सत्यापन सफल`, from: '+15818900669', to: '+917889152578'})
                  .then(message => {
                    console.log('Otp verified message sent');
                  })
              return res.status(200).json({
                status: true,
                info: "OTP Verified successfully",
              });
            })
      }else {
        return res.status(404).json({
          status: false,
          info: "Incorrect otp",
        });
      }
    }
  }catch (e) {
    console.log(e);
    return res.status(401).json({
      status: false,
      info: "Exception: Failed to verify OTP",
    });
  }
}

exports.resetPassword = async function (req, res) {
  console.log("Reset password start");
  console.log(req.body);
  if (!req.body.token || !req.body.pswd) {
    return res.status(401).json({
      status: false,
      info: "Incorrect Credentials provided",
    });
  }
  let ans = await tokens.findById(req.body.token).lean();
  console.log(ans);
  if (ans == null) {
    console.log("Incorrect token");
    return res.status(401).json({
      status: false,
      info: "Reset token not valid",
    });
  } else {
    //checking if the token is expired,
    if (new Date(ans.expiry) < Date.now()) {
      console.log("Token has been expired");
      return res.status(404).json({
        status: false,
        info: "Reset token has been EXPIRED",
      });
    }
    console.log("Token correct, reset the pasword");
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(req.body.pswd, salt, async function (err, hash) {
        // Storing the updated password in db
        let update = await ngUser.updateOne(
          { email: ans.email },
          { pswd: hash }
        );
        console.log(update);
        tokens.findByIdAndDelete(req.body.token).then((del) => {
          console.log("Cur token deleted.");
        });
        return res.status(202).json({
          status: false,
          info: "Password reset successfully",
        });
      });
    });
  }
};

exports.genResetToken = async function (req, res) {
  console.log("Generate password refresh token and mail");
  try {
    let ans = await ngUser.findOne({ email: req.body.email }).lean();
    if (ans == null) {
      console.log("User not found");
      return res.status(401).json({
        status: false,
        info: "No account is associated with this email !",
      });
    } else {
      console.log("User found, sending the link to email associated");
      console.log("Request to send Email recieved");
      let tokenExpires = new Date().setMinutes(new Date().getMinutes() + 10);
      //valid for ten minutes only
      let nayaReset = new tokens({
        email: req.body.email,
        expiry: tokenExpires,
      });
      nayaReset
        .save()
        .then((resp) => {
          console.log("token saved ");
          console.log(resp);
          let token = resp._id;
          let link = `https://angular2312.herokuapp.com/auth/resetPassword/` + token;
          let stringExp = new Date(tokenExpires).toLocaleString("en-IN");
          console.log(stringExp);
          let message = {
            to: req.body.email, // list of receivers
            from: "www.avinashkumar2@gmail.com", // sender address
            subject: "Hello link to reset password ✔", // Subject line
            text: "Hello This is your test mail", // plain text body
            html: `
                <h1>Hello Here is your link to reset your password associated with your account</h1>
                <h3><b>This link is valid for 10 minutes, till ${stringExp}</b></h3>
                <p>Link: ${link}</p>
                `,
          };
          sgMail
            .send(message)
            .then((response) => {
              console.log("mail send success");
              return res.status(202).json({
                res: response,
                status: true,
                info: "Reset link sent successfully, check your mail",
              });
            })
            .catch((err) => {
              console.log(err);
              return res.status(401).json({
                status: false,
                info: "Exception: Failed to send mail",
              });
            });
        })
        .catch((err) => {
          console.log("error saving the token");
          return res.status(401).json({
            status: false,
            info: "Exception: Failed to send mail",
          });
        });
    }
  } catch (err) {
    console.log("exception: Email find");
    return res.status(401).json({
      status: false,
      info: "Exception: Failed to send mail",
    });
  }
};

exports.googleOauthLogin = async function (req, res) {
  //verifying the access token recieved from the backend;
  console.log("Verifying the access token");
  try {
    const response = await axios.get(
      "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" +
        req.body.accessToken
    );
    console.log(response.data.verified_email);
    console.log("Email: " + response.data.email);
    console.log("Fname: " + response.data.given_name);
    console.log("Fname: " + response.data.family_name);
    //checking of the data is correct and auth also gives correct token
    if (response.data.email && response.data.email === req.body.email) {
      console.log("Data is valid");
      //find the email, and send the data
      try {
        let user = await ngUser.findOne({ email: response.data.email }).lean();
        console.log(user);
        if (user === null || !user) {
          console.log("User not found, registering user, and send for login");

          let nayaNgUser = new ngUser({
            fname: response.data.given_name,
            lname: response.data.family_name,
            userName: response.data.email,
            email: response.data.email,
            pswd: "Oauth-Login",
            isOauth: true,
          });
          nayaNgUser
            .save()
            .then((resData) => {
              console.log("New use saved , redirect for login");
              jwt.sign(
                { userId: response.data.email },
                "mysecretKey",
                (err, token) => {
                  console.log(err, token);
                  if (err) {
                    return res.status(401).json({
                      status: false,
                      info: "JWT failed !",
                      loginRedirect: false,
                    });
                  }
                  res.cookie("auth", token, {
                    maxAge: 24 * 60 * 60 * 1000,
                    httpOnly: false,
                    secure: false,
                  });

                  return res.status(200).json({
                    jwt: token,
                    info: "New user saved, Jwt signed login sucess, redirect profile",
                    loginRedirect: true,
                    isMobileVerified:false,
                    //for new user it is already false
                  });
                }
              );
            })
            .catch((err) => {
              console.log(err);
              res.status(400).end();
            });
        } else {
          console.log("User exist send the profile details to login");

          jwt.sign({ userId: user.userName }, "mysecretKey", (err, token) => {
            console.log(err, token);
            if (err) {
              return res.status(401).json({
                status: false,
                info: "JWT failed !",
                loginRedirect: false,
              });
            }
            res.cookie("auth", token, {
              maxAge: 24 * 60 * 60 * 1000,
              httpOnly: false,
              secure: false,
            });

            res.status(200).json({
              jwt: token,
              info: "Jwt signed login sucess, redirect profile",
              loginRedirect: true,
              isMobileVerified:user.mobileVerified??false,
            });
          });
        }
      } catch (err) {
        console.log(err);
        res.status(404).json({
          verified: false,
          loginRedirect: false,
          message: "Error finding the user in DB",
        });
      }
    } else {
      console.log("Data didnt matched");
      res.status(404).json({
        verified: false,
        loginRedirect: false,
        message: "Wrong access token send",
      });
    }
  } catch (error) {
    console.error(error);
    res.status(400).end();
  }
};
exports.registerUser = function (req, res) {
  console.log("Registering User");
  console.log(req.body);

  bcrypt.genSalt(10, function (err, salt) {
    bcrypt.hash(req.body.pswd, salt, function (err, hash) {
      console.log("pswd hashed");
      if (err) {
        console.log(err);
        res.status(400).end();
      } else {
        console.log("Running hash OK");

        let nayaNgUser = new ngUser({
          fname: req.body.fname,
          lname: req.body.lname,
          userName: req.body.userName,
          email: req.body.email,
          pswd: hash, //we store the hashed and salted password for security
        });
        nayaNgUser
          .save()
          .then((resData) => {
            console.log(resData);
            smsClient.messages
                .create({body: `Hi, ${req.body.fname},Welcome to Online Resume Builder, you account has been created, you need to verify your mobile number on login | ऑनलाइन रिज्यूमे बिल्डर में आपका स्वागत है, आपका खाता बन गया है, आपको लॉगिन पर अपना मोबाइल नंबर सत्यापित करना होगा`, from: '+15818900669', to: '+917889152578'})
                .then(message => {
                  console.log('Registration  message sent');
                })
            jwt.sign(
              { userId: req.body.userName },
              "mysecretKey",
              (err, token) => {
                console.log(err, token);
                if (err) {
                  return res.status(401).json({
                    status: false,
                    info: "JWT failed !",
                  });
                }
                res.cookie("auth", token, {
                  maxAge: 24 * 60 * 60 * 1000,
                  httpOnly: false,
                  secure: false,
                });

                res.status(200).json({
                  jwt: token,
                  info: "Jwt SignUp success and sent to client",
                  status: true,
                });
              }
            );
          })
          .catch((err) => {
            console.log(err);
            res.status(400).end();
          });
      }
    });
  });
};

exports.loginUser = function (req, res) {
  console.log("logging in  User");
  console.log(req.body); //username and password
  console.log(req.header);
  console.log("Cookies: ");
  console.log(req.cookies);
  ngUser
    .findOne({ userName: req.body.userName })
    .lean()
    .then((data) => {
      console.log(data);
      if (data == null) {
        return res.status(404).json({
          status: false,
          info: "ERROR! User NOT found",
        });
      }
      if (data.isOauth) {
        return res.status(404).json({
          status: false,
          info: "Please Login using Oauth",
        });
      }
      //match the provided user password and provide the auth tocken
      bcrypt.compare(req.body.pswd, data.pswd, (err, ans) => {
        if (ans === true) {
          console.log("User password matched successs");
          //now we will sign the jwt token with the userName
          jwt.sign(
            { userId: req.body.userName },
            "mysecretKey",
            (err, token) => {
              console.log(err, token);
              if (err) {
                return res.status(401).json({
                  status: false,
                  info: "Retry! JWT failed",
                });
              }
              res.cookie("auth", token, {
                maxAge: 24 * 60 * 60 * 1000,
                httpOnly: false,
                secure: false,
              });

              res.status(200).json({
                jwt: token,
                info: "Jwt signed and sent to client",
                status: true,
                isMobileVerified:data.mobileVerified??false,
              });
            }
          );
        }
        //password not match
        else {
          return res.status(401).json({
            status: false,
            info: "Authentication failed !",
          });
        }
        if (err) {
          return res.status(404).json({
            status: false,
            info: "Error in verifying Password !",
          });
        }
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(404).json({
        status: false,
        info: "User found ERROR",
      });
    });
};

exports.checkUserName = function (req, res) {
  let userId = req.params.uid;
  console.log(userId);
  ngUser
    .findOne({ userName: userId })
    .lean()
    .then((data) => {
      console.log(data);
      if (data == null) {
        return res.status(200).json({
          userFound: false,
          availableUser: true,
          status: false,
          info: "User NOT FOUND ",
        });
      }
      //if user is available we cant use the user Name
      res.status(200).json({
        userFound: true,
        availableUser: false,
      });
    })
    .catch((Err) => {
      console.log(Err);
      res.status(404).json({
        status: false,
        info: "User FOUND ERROR",
      });
    });
};

exports.profile = function (req, res) {
  console.log(req.cookies);
  console.log(req.headers["authorization"]);
  console.log(req["cookies"]["auth"]);

  console.log("Api secure request rec");
  console.log(req["cookies"]["sasa"]);
  if (req.headers["authorization"] === undefined) {
    return res.status(401).json({
      redirect: true,
      location: "login",
      message: "Auth token not found",
    });
  }

  jwt.verify(
    req.headers["authorization"].slice(1, -1),
    "mysecretKey",
    (err, authData) => {
      console.log("Inside jwt");
      console.log(authData.userId);
      if (err) {
        console.log(err);
        res.sendStatus(404);
      } else {
        ngUser
          .findOne({ userName: authData.userId })
          .lean()
          .then((data) => {
            console.log(data);
            if (data == null) {
              return res.status(404).json({
                userFound: false,
                status: false,
                info: "User NOT FOUND ",
              });
            }
            //if user is available we cant use the user Name
            res.status(200).json({
              fname: data.fname,
              lname: data.lname,
              email: data.email,
              userName: data.userName,
              isMobileVerified:data.mobileVerified??false,
              userFound: true,
              availableUser: false,
              isOauth: data.isOauth ?? false,
            });
          })
          .catch((Err) => {
            console.log(Err);
            res.status(404).json({
              status: false,
              info: "User FOUND ERROR",
            });
          });
      }
    }
  );
};

exports.updateProfile = function (req, res) {
  console.log(req.body);
  if (req.headers["authorization"] === undefined) {
    return res.status(404).json({
      status: false,
      message: "Auth token not available",
    });
  }

  jwt.verify(
    req.headers["authorization"].slice(1, -1),
    "mysecretKey",
    (err, authData) => {
      if (err) {
        console.log(err);
        res.status(404).json({
          status: false,
          message: "Auth token not available",
        });
      } else {
        console.log(authData);
        console.log(String(authData.userId));
        //hashing the password
        bcrypt.genSalt(10, function (err, salt) {
          bcrypt.hash(req.body.pswd, salt, function (err, hash) {
            console.log("pswd hashed");
            if (err) {
              console.log(err);
              res.status(400).end();
            } else {
              console.log("Running hash OK");
              //*****Earlier we were updating the userName, now we will not change the username
              //updating all resume userName of this user
              // ngCV
              //   .updateMany(
              //     { userName: authData.userId },
              //     { userName: req.body.userName }
              //   )
              //   .then((upRes) => {
              //     console.log("All resume of this user updated");
              //   });
              //updating user data
              console.log("Updating user profile data: "+authData.userId)
              ngUser
                .updateOne(
                  { userName: authData.userId },
                  {
                    fname: req.body.fname,
                    lname: req.body.lname,
                    email: req.body.email,
                    pswd: hash,
                  },
                  { new: true }
                )
                .then((dta) => {
                  console.log(dta);
                  res.status(200).json({
                    info: "Profile Updated successfully",
                    status: true,
                  });
                })
                .catch((error) => {
                  console.log('Erorr updating user profile')
                  console.log(error)
                  res.status(404).json({
                    status: false,
                    message: "Error updating user",
                    err: error,
                  });
                });
            }
          });
        });
      }
    }
  );
};

exports.getCVdet = async function (req, res) {
  if (req.headers["authorization"] === undefined) {
    return res.status(404).json({
      status: false,
      message: "Auth token not available",
    });
  }
  console.log("request to get all user cv details");
  try {
    var decoded = jwt.verify(
      req.headers["authorization"].slice(1, -1),
      "mysecretKey"
    );
    console.log(decoded);
    let curUid = decoded.userId;
    let allResume = await ngCV.find({ userName: curUid }).lean();
    console.log(allResume);
    res.status(201).json(allResume);
  } catch (err) {
    return res.status(404).json({
      status: false,
      message: "Error in Jwt tocken",
    });
  }
};

exports.getOneCV = async function (req, res) {
  if (req.headers["authorization"] === undefined) {
    return res.status(404).json({
      status: false,
      message: "Auth token not available",
    });
  }
  console.log("request to get cv details");
  let allResume = await ngCV.findById(req.body._id).lean();
  console.log(allResume);
  res.json(allResume);
};

exports.updateResume = async function (req, res) {
  console.log("Update request resume");
  console.log(req.body);
  if (req.headers["authorization"] === undefined) {
    return res.status(404).json({
      status: false,
      message: "Auth token not available",
    });
  }
  try {
    var decoded = jwt.verify(
      req.headers["authorization"].slice(1, -1),
      "mysecretKey"
    );
    console.log(decoded);
    req.body.data["userName"] = decoded.userId;
    console.log("Data going to be saved");
    console.log(req.body.data);
    //update the resume by its id
    let updtRes = await ngCV.updateOne({ _id: req.body.curId }, req.body.data);
    console.log("resume Update run succ");
    console.log(updtRes);
    res.status(201).json(updtRes);
  } catch (err) {
    return res.status(404).json({
      status: false,
      message: "Error in Jwt tocken",
    });
  }
};
exports.newResume = function (req, res) {
  console.log("New resume insert request");
  console.log(req.body);
  if (req.headers["authorization"] === undefined) {
    return res.status(404).json({
      status: false,
      message: "Auth token not available",
    });
  }
  try {
    var decoded = jwt.verify(
      req.headers["authorization"].slice(1, -1),
      "mysecretKey"
    );
    console.log(decoded);
    req.body.data["userName"] = decoded.userId;
    console.log(req.body.data);
    let nayaRes = new ngCV(req.body.data);

    nayaRes
      .save()
      .then((data) => {
        console.log(data);
        res.status(201).json(data);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json(err);
      });
  } catch (err) {
    return res.status(404).json({
      status: false,
      message: "Error in Jwt tocken",
    });
  }
};

exports.deleteResume = async function (req, res) {
  console.log("Delete request for CV recieved");
  console.log(req.body);
  let delResp = await ngCV.deleteOne({ _id: req.body._id });
  console.log(delResp);
  res.status(200).json({ success: delResp });
};

//testing file upload
exports.fileUpload = function (req, res) {
  console.log("File up test");
  console.log(req.body);
  res.status(202).json({ success: true });
};
