var express = require("express");
var router = express.Router();
var secureController = require("../controllers/secureNg");
var multer = require("multer");
//for pdf parse
let fs = require("fs");
const emailUtil = require("../email-util");
const { sendEmail } = emailUtil;
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./temp/fileUploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
var upload = multer({ storage: storage, limits: { fileSize: 100000 } });

router.get("/", (req, res) => {
  console.log("Req for main secure");
  res.json({
    response: "This is my data",
    success: true,
  });
});

router.post("/register", secureController.registerUser);
router.post("/login", secureController.loginUser);
router.get("/checkUserId/:uid", secureController.checkUserName);
router.get("/profile", secureController.profile);
router.post("/update", secureController.updateProfile);
router.post("/googleLogin", secureController.googleOauthLogin);
router.post("/resetPassword", secureController.resetPassword);
router.post("/genResetToken", secureController.genResetToken);
router.post('/sendSMSotp',secureController.sendSMS);
router.post('/verifyOtp',secureController.verifyOtp);


//routes for the CV
router.get("/cvinfo", secureController.getCVdet);
router.post("/cvSingle", secureController.getOneCV);
router.post("/updateResume", secureController.updateResume);
router.post("/newResume", secureController.newResume);
router.post("/deleteResume", secureController.deleteResume);
router.post(
  "/fileUpload",
  // upload.single("myfile"),
  secureController.fileUpload
);

router.get("/logout", (req, res) => {
  console.log("cookies deleted");
  res.clearCookie("auth");
  console.log(req["cookies"]);
  res.status(200).json({
    logout: true,
  });
});

router.get("/isAuth", (req, res) => {
  jwt.verify(req.token, "mysecretKey", (err, authData) => {
    if (err) {
      console.log(err);
      res.sendStatus(404);
    } else {
      res.json({
        message: "Secure access",
        data: authData,
      });
    }
  });
});
router.get("/setCookie/:key", (req, res) => {
  res.cookie("value", req.params.key);
  res.send(req.cookies);
});

router.get("/getCookie", (req, res) => {
  console.log(req.cookies);
  res.send(req.cookies);
});

//testing the pdf parse
router.get("/parsePdf", (req, res) => {
  console.log("Parsing pdf start");
});

router.get("/sendMail", async (req, res) => {
  const { recipient, message } = req.body;
  try {
    console.log("Start sending mail");
    let ans = await sendEmail("recipient", "message");
    console.log(ans);
    res.json({ message: "Your query has been sent" });
  } catch (e) {
    console.log("Error sending mail");
    console.log(e);
  }
});
module.exports = router;

/*
let dataBuffer = fs.readFileSync("./temp/pdfFiles/resume.pdf");

  pdf(dataBuffer).then(function (data) {
    // number of pages
    console.log(data.numpages);
    // number of rendered pages
    console.log(data.numrender);
    // PDF info
    console.log(data.info);
    // PDF metadata
    console.log(data.metadata);
    // PDF.js version
    // check https://mozilla.github.io/pdf.js/getting_started/
    console.log(data.version);
    // PDF text
    console.log(data.text);
    res.send(data.text);
  });

  ////////seond way
  let i = 0;

  var rows = {}; // indexed by y-position
  new pdfreader.PdfReader().parseFileItems(
    "./temp/pdfFiles/resume.pdf",
    function (err, item) {
      if (!item || item.page) {
        // end of file, or page
        printRows();
        rows = {}; // clear rows for next page
      } else if (item.text) {
        (rows[item.y] = rows[item.y] || []).push(item.text);
      }
    }
  );
  function printRows() {
    console.log("printing values: " + i++);
    Object.keys(rows) // => array of y-positions (type: float)
      .sort((y1, y2) => parseFloat(y1) - parseFloat(y2)) // sort float positions
      .forEach((y) => console.log(`[${y}]: [ ${(rows[y] || []).join("")}]`));
    console.log("round: " + i);
    if (i === 2) res.json(rows);
  }


  //third way
  let dataBuffer = fs.readFileSync("./temp/pdfFiles/JONATHAN_MA_RESUME.pdf");

  pdf(dataBuffer).then(function (data) {
    // number of pages
    console.log(data.numpages);
    // number of rendered pages
    console.log(data.numrender);
    // PDF info
    console.log(data.info);
    // PDF metadata
    console.log(data.metadata);
    // PDF.js version
    // check https://mozilla.github.io/pdf.js/getting_started/
    console.log(data.version);
    // PDF text
    console.log(data.text);
    res.json(data.text);
  });
 */
