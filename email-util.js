const emailConfig = require("./email-config")();
const mailgun = require("mailgun-js")(emailConfig);
exports.sendEmail = (recipient, message, attachment) =>
  new Promise((resolve, reject) => {
    const data = {
      from: "Gobinda Thakur <info@mg.gobindathakur.com>",
      to: "www.avinashkumar2@gmail.com",
      subject: "Test subject",
      text: "Apple is red",
    };
    mailgun.messages().send(data, (error) => {
      if (error) {
        return reject(error);
      }
      return resolve();
    });
  });
