var mongoose = require("mongoose");

var resumeSchema = mongoose.Schema(
  {
    userName: { type: String, required: true },
    resumeName: { type: String },
    basicDetails: {
      firstName: { type: String },
      lastName: { type: String },
      profession: { type: String },
      city: { type: String },
      state: { type: String },
      pinCode: { type: String },
      phone: { type: String },
      email: { type: String },
    },
    socialLink: {
      linkedInLink: { type: String },
      gitHubLink: { type: String },
    },
    experiences: [
      {
        jobTitle: { type: String },
        employer: { type: String },
        city: { type: String },
        startDate: { type: Date },
        endDate: { type: Date },
        desc: [{ type: String }],
      },
    ],
    projects: [
      {
        projectName: { type: String },
        link: { type: String },
        startDate: { type: Date },
        endDate: { type: Date },
        desc: [{ type: String }],
      },
    ],
    education: [
      {
        schoolName: { type: String },
        location: { type: String },
        degree: { type: String },
        fieldOfStudy: { type: String },
        startDate: { type: Date },
        endDate: { type: Date },
        marks: { type: Number },
      },
    ],
    skills: [
      {
        name: { type: String },
        level: { type: Number }, //on a scale of 0-5
        description: { type: String },
      },
    ],
    accomplishments: [
      {
        name: { type: String },
        description: { type: String },
        dateOfAcc: { type: Date },
      },
    ],
    hobbies: [{ type: String }],
    certification: [
      {
        name: { type: String },
        description: { type: String },
        link: { type: String },
        date: { type: Date },
      },
    ],

    //end of schema
  },
  { collection: "resume" }
);

module.exports = mongoose.model("resume", resumeSchema);
