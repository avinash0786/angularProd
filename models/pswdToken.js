var mongoose = require("mongoose");
var token = mongoose.Schema(
  {
    email: {
      type: String,
    },
    expiry: {
      type: Date,
    },
  },
  { collection: "pswdToken" }
);
module.exports = mongoose.model("pswdToken", token);
